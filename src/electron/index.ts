import { app, BrowserWindow } from "electron";
import * as path from "path";
// import url from "url";
const mode = process.env.NODE_ENV;
try {
    if (mode === "development")
        require("electron-reload")(__dirname, {
            electron: path.join(__dirname, "node_modules", "electron"),
        });
} catch (error) {}

async function createWindow() {
    // Create the browser window.
    let mainWindow = new BrowserWindow({
        height: 600,
        webPreferences: {
            preload: path.join(__dirname, "preload.js"),
            contextIsolation: true
        },
        width: 800,
    });

    const url =
        mode !== "development"
            ? // in production, use the statically build version of our application
              `file://${path.join(__dirname, "..", "index.html")}`
            : // in dev, target the host and port of the local rollup web server
              "http://localhost:5000";

    // mainWindow.loadURL(`file://${path.join(__dirname, "..", "index.html")}`);
    mainWindow.loadURL(url);

    let watcher: any;
    if (mode === "development") {
        console.log("DEVELOPMENT MODE");
        // setTimeout(() => {
        //     mainWindow.reload();
        // }, 6000);
        watcher = require("chokidar").watch(
            path.join(__dirname, "../public/"),
            {
                ignoreInitial: true,
            }
        );
        watcher.on("change", () => {
            mainWindow.reload();
        });
    }

    mainWindow.on("closed", () => {
        mainWindow = null;
        if (watcher) {
            watcher.close();
        }
    });

    // Open the DevTools.
    // mainWindow.webContents.openDevTools();
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", () => {
    createWindow();

    app.on("activate", function () {
        // On macOS it's common to re-create a window in the app when the
        // dock icon is clicked and there are no other windows open.
        if (BrowserWindow.getAllWindows().length === 0) createWindow();
    });
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.
