const purgecss = require('@fullhuman/postcss-purgecss')({
  content: [
    './src/**/*.ts',
    './src/**/*.svelte'
  ],

  whitelistPatterns: [/svelte-/],

  defaultExtractor: (content) => content.match(/[A-Za-z0-9-_:/]+/g) || []
});

const production = !process.env.ROLLUP_WATCH;

module.exports = {
  plugins: [
    require( 'tailwindcss' ),
    require( "autoprefixer" ),
    require( "postcss-nested" ),
    ...(production ? [purgecss] : [])
  ]
};