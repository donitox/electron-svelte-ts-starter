module.exports = {
    purge: [
        './src/**/*.ts',
        './src/**/*.svelte',
    ],
    darkMode: "class", // or 'media' or 'class'
    theme: {
        extend: {},
    },
    variants: {},
    plugins: [],
}
